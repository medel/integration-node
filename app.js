const express = require('express')
const app  = express()

app.get('/', (req, res) => {
    res.json({ "status": "work!" })
})

module.exports = app
